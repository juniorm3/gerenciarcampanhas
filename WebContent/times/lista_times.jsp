<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<h3>LISTA DE TIMES</h3>

<c:if test="${times.size() == 0 }">
	<br><h4>N�o existem times cadastrados!</h4><br>
</c:if>
<c:if test="${times.size() > 0 }">
	<table>
		<c:forEach var="t" items="${times}">
			<tr>
				<td>${t.nome}</td>								
				<td><a href="TimeForm.action?id=${t.id}">Alterar</a> | <a href="ExcluirTime.action?id=${t.id}">Excluir</a></td>
			</tr>
		</c:forEach>
	</table>
</c:if>

<a href="TimeForm.action">Cadastrar novo Time</a>

<jsp:include page="footer.jsp" />