<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<h3>LISTA DE CAMPANHAS</h3>

<c:if test="${campanhas.size() == 0 }">
	<br><h4>N�o existem campanhas cadastradas!</h4><br>
</c:if>
<c:if test="${campanhas.size() > 0 }">
	<table>
		<c:forEach var="c" items="${campanhas}">
			<tr>
				<td>${c.nome}</td>
				<td><fmt:formatDate type="date" value="${c.inicio}"/></td>				
				<td><fmt:formatDate type="date" value="${c.fim}"/></td>
				<td>${c.timeCoracao.nome}</td>
				<td><a href="CampanhaForm.action?id=${c.id}">Alterar</a> | <a href="ExcluirCampanha.action?id=${c.id}">Excluir</a></td>
			</tr>
		</c:forEach>
	</table>
</c:if>

<a href="CampanhaForm.action">Cadastrar nova Campanha</a>

<jsp:include page="footer.jsp" />