<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<h3>PESQUISA DE CAMPANHAS ATIVAS</h3>
<c:if test="${erros != null}">
	<c:forEach var="erro" items="${erros}">
		<span class="erro">${erro}</span><br>
	</c:forEach>
</c:if>
<form action="PesquisarCampanhas.action" method="post">
<table>
	<tr>
		<td>Nome:</td>
		<td><input type="text" name="nome" size="40" /></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" value="Pesquisar" class="button" /></td>
	</tr>
</table>
</form>

<c:forEach var="campanha" items="${campanhas}">
	<span class="livro_titulo">${campanha.nome}</span><br>
	<span class="livro_info">TIME DO CORA��O: ${campanha.timeCoracao.nome}</span><br>
	<span class="livro_info">INICIO: <fmt:formatDate type="date" value="${campanha.inicio}"/></span><br>
	<span class="livro_info">FIM: <fmt:formatDate type="date" value="${campanha.fim}"/></span><br><br>
	<A href="VincularCampanha.action?op=inserir&id=${campanha.id}">Vicular esta campanha</A><br><br>
</c:forEach>
<jsp:include page="footer.jsp" />