<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<h3>USU�RIO</h3>

<c:if test="${erros != null}">
	<c:forEach var="erro" items="${erros}">
		<span class="erro">${erro}</span><br>
	</c:forEach>
</c:if>

<form action="SalvarUsuario.action" method="post">
<input type="hidden" name="id" value="${id}">
<table>
	<tr>
		<td>Usu�rio:</td>
		<td><input type="text" name="nome" size="40" value="${nome}"/></td>
	</tr>
	<tr>
		<td>Email:</td>
		<td><input type="text" name="email" size="40"  value="${email}" /></td>		
	</tr>
	<tr>
		<td>Nascimento:</td>
		<td><input type="text" name="nascimento" size="40" placeholder="Ex.: dd/mm/aaaa" maxlength="10" value="<fmt:formatDate type="date" value="${nascimento}"/>" onKeyPress="DataHora(event, this)"/></td>
	</tr>
	<tr>
		<td>Time do Cora��o:</td>
		<td><select name="timeCoracao" >				
				<option value="-1">Selecione um Time</option>
				<c:forEach items="${times}" var="time">
					<c:if test="${timeCoracao == time.id }">
						<option value="${time.id}" selected="selected">${time.nome}</option>
					</c:if>
					<c:if test="${timeCoracao != time.id }">
						<option value="${time.id}" >${time.nome}</option>
					</c:if>
				</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<td>Campanhas:</td>
		<td>
			<ul>
				<c:forEach items="${campanhas}" var="camapanha">
					<li>${campanha.nome}</li>
				</c:forEach>
			</ul>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" value="Salvar" class="button" /></td>
	</tr>
</table>
</form>

<jsp:include page="footer.jsp" />