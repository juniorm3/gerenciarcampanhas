<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<h3>LISTA DE USU�RIOS</h3>

<c:if test="${usuarios.size() == 0 }">
	<br><h4>N�o existem usuarios cadastrados!</h4><br>
</c:if>
<c:if test="${usuarios.size() > 0 }">
	<c:forEach var="u" items="${usuarios}">
		<span class="livro_titulo">Usu�rio: ${u.nome}</span><br>
		<span class="livro_titulo">E-Mail: ${u.email}</span><br>
		<span class="livro_titulo">Dt Nascimento: <fmt:formatDate type="date" value="${u.nascimento}"/></span><br>
		<span class="livro_titulo">Time Cora��o: ${u.timeCoracao.nome}</span><br>
		<ul>
			<c:forEach items="${u.campanhas}" var="camapanha">
				<li>${campanha.nome}</li>
			</c:forEach>
		</ul><br>			
		<a href="UsuarioForm.action?id=${u.id}">Alterar</a> | <a href="ExcluirUsuario.action?id=${u.id}">Excluir</a> <br><br>			
	</c:forEach>	
</c:if>

<a href="UsuarioForm.action">Cadastrar novo Usuario</a>

<jsp:include page="footer.jsp" />