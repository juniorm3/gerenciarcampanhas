package campanhas.tools;

import java.util.Calendar;
import java.util.Date;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import campanhas.dao.CampanhaDAO;
import campanhas.dao.DAOFactory;
import campanhas.dao.TimeDAO;
import campanhas.dao.UsuarioDAO;
import campanhas.entity.Campanha;
import campanhas.entity.Time;
import campanhas.entity.Usuario;
import campanhas.util.HibernateUtil;

public class DBCreator {

	public static void main(String[] args) throws Exception {
		Configuration cfg = new Configuration().configure();
		SchemaExport schemaExport = new SchemaExport(cfg);

		// Exclui as tabelas
		schemaExport.drop(true, true);

		// Cria as Tabelas
		schemaExport.create(true, true);

		// Insere os dados
		insertData();
	}

	private static void insertData() throws Exception {
		try {
			DAOFactory daoFactory = DAOFactory.getInstance();
			HibernateUtil.beginTransaction();
			

			TimeDAO timeDAO = daoFactory.getDAO(TimeDAO.class);
			CampanhaDAO campanhaDAO = daoFactory.getDAO(CampanhaDAO.class);
			UsuarioDAO usuarioDAO = daoFactory.getDAO(UsuarioDAO.class);

			Time t = new Time();
			t.setNome("Corinthians");			
			timeDAO.save(t);

			t = new Time();
			t.setNome("São Paulo");
			timeDAO.save(t);

			t = new Time();
			t.setNome("Santos");
			timeDAO.save(t);
			
			t = new Time();
			t.setNome("Palmeiras");
			timeDAO.save(t);

			Calendar cl = Calendar.getInstance();
			
			Campanha c = new Campanha();
			c.setNome("Dia dos Pais");								
			cl.set(2017, 07, 20);
			Date incio = cl.getTime();
			c.setInicio(incio);			
			cl.set(2017, 07, 30);
			Date fim = cl.getTime();
			c.setFim(fim);
			c.setTimeCoracao(t);
			campanhaDAO.save(c);
			
			Usuario u = new Usuario();
			u.setNome("Mario");
			u.setEmail("juniorm3@msn.com");			
			cl.set(1977, 02, 26);
			Date nascimento = cl.getTime();
			u.setNascimento(nascimento);			
			u.setTimeCoracao(t);
			usuarioDAO.save(u);

			HibernateUtil.commitTransaction();
		} catch (Exception e) {
			HibernateUtil.rollbackTransaction();
			throw e;
		}

	}

}
