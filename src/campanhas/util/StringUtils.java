package campanhas.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtils {

	public static boolean isEmpty(String str) {
		if (str == null) {
			return true;
		}
		
		return str.trim().length() == 0;
	}
	
	public static boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public static boolean isDouble(String str) {
		if (str == null) {
			return false;
		}
		
		try {
			Double.parseDouble(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	/**
     * Converte uma data em string
     * @param Date - data
     * @param String - formto para conversão da data
     * @return String - string com a data
     */
    public static String convertDate(Date date, String format) {
        if(date != null) {
            SimpleDateFormat sdf = format != null ? new SimpleDateFormat(format) : new SimpleDateFormat("dd/MM/yyyy");
            sdf.setLenient(false);
            return sdf.format(date);
        } else {
            return "";
        }
    }

    /**
     * Converte uma string em uma data
     * @param String - data
     * @param String - formato ( default = 'dd/MM/yyyy' )
     * @return Date
     * @throws ParseException 
     */
    public static Date stringToDate(String data,String formato) throws ParseException{
        Date date = null;
        if ( formato ==null) formato = "dd/MM/yyyy";
            DateFormat formatter;
            formatter = new SimpleDateFormat(formato);
            date = (Date) formatter.parse(data);
            String s = formatter.format(date);

        return date;
    } 
}
