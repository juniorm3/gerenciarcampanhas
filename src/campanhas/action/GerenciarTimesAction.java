package campanhas.action;

import java.util.List;

import campanhas.entity.Time;
import campanhas.service.TimeService;

public class GerenciarTimesAction extends Action {

	@Override
	public void process() throws Exception {
		
		getSession().setAttribute("menuAtivo", "times");
		
		TimeService timesService = serviceFactory.getService(TimeService.class);
		List<Time> times = timesService.getTimes();
		
		getRequest().setAttribute("times", times);
		
		forward("times/lista_times.jsp");
		
	}

}
