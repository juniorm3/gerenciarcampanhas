package campanhas.action;

import java.util.List;

import campanhas.entity.Campanha;
import campanhas.service.CampanhaService;

/**
 * Pesquisa campanhas exixtentes
 * @author Mario
 *
 */
public class PesquisarCampanhasAction extends Action{

	@Override
	public void process() throws Exception {
		
		String nome = getRequest().getParameter("nome");
		
		if(nome == null) {
			getSession().setAttribute("menuAtivo", "campanhas");
			forward("campanha/pesquisar_campanhas.jsp");
			return;
		}
		
		CampanhaService campanhaService = serviceFactory.getService(CampanhaService.class);
		// Realiza a pesquisa
		List<Campanha> campanhas;
		if(nome.isEmpty() || nome.equals("")) {
			campanhas = campanhaService.getCampanhasValidas();
		} else {		
			campanhas = campanhaService.pesquisarCampanhasValidas(nome);	
		}				
		
		getRequest().setAttribute("campanhas", campanhas);
		forward("campanha/pesquisar_campanhas.jsp");
		
	}

	
}
