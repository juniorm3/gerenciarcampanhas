package campanhas.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import campanhas.entity.Time;
import campanhas.entity.Usuario;
import campanhas.service.ServiceException;
import campanhas.service.TimeService;
import campanhas.service.UsuarioService;
import campanhas.util.StringUtils;
import campanhas.util.ValidaUtils;

public class SalvarUsuarioAction extends Action{

	@Override
	public void process() throws Exception {
		
		/*Recupera dados informados do formulario*/
		String id = getRequest().getParameter("id").trim();
		String nome = getRequest().getParameter("nome").trim();
		String email = getRequest().getParameter("email").trim();
		String nascimento = getRequest().getParameter("nascimento").trim();
		String timeCoracao = getRequest().getParameter("timeCoracao").trim();
		
		/*Valida os dados*/
		validarNome(nome);
		validarEmail(email);
		validarNascimento(nascimento);
		validarTime(timeCoracao);
		
		Date dtNascimento = null;
		if(!existemErros()) {
			dtNascimento = StringUtils.stringToDate(nascimento, "dd/MM/yyyy");	
		}
		 
		
		/*Caso existam erros recoloca os dados na request e retorna para a mesma pagina*/
		if(existemErros()) {
			definirValores(id, nome, email, dtNascimento, timeCoracao);
			forward("usuarios/usuario.jsp");
			return;
		}
		
		Usuario usuario = new Usuario();
		
		if(!StringUtils.isEmpty(id)) {
			usuario.setId(Integer.parseInt(id));
		}
		usuario.setNome(nome);
		
		
		usuario.setEmail(email);		
		usuario.setNascimento(dtNascimento);
		
		
		TimeService timeService = serviceFactory.getService(TimeService.class);
		Time time = timeService.getTimeById(Integer.parseInt(timeCoracao));
		usuario.setTimeCoracao(time);
				
		/*Grava dados*/
		UsuarioService usuarioService = serviceFactory.getService(UsuarioService.class);
		usuarioService.salvar(usuario);
		
		redirect("GerenciarUsuario.action");
		
	}
		

	/**
	 * Recoloca os dados preenchidos na request, para permitir que o formulário 
	 * fique preenchido quando a tela for recarregada
	 * @param id
	 * @param nome
	 * @param email
	 * @param dtNascimento
	 * @param timeCoracao
	 * @throws ServiceException 
	 */
	private void definirValores(String id, String nome, String email, Date dtNascimento, String timeCoracao) throws ServiceException {		
		getRequest().setAttribute("id", id);
		getRequest().setAttribute("nome", nome);
		getRequest().setAttribute("email", email);
		getRequest().setAttribute("nascimento", dtNascimento);
		getRequest().setAttribute("timeCoracao", Integer.parseInt(timeCoracao));

		TimeService timeService = serviceFactory.getService(TimeService.class);
		List<Time> times = timeService.getTimes();
		getRequest().setAttribute("times", times);
	}

	/**
	 * Verifica se existem erros de validados
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private boolean existemErros() {
		List<String> erros = (List<String>) getRequest().getAttribute("erros");
		if (erros == null || erros.size() == 0) {
			return false;
		}
		return true;
	}


	private void validarTime(String timeCoracao) {
		Integer id = Integer.parseInt(timeCoracao);
		if(id == -1) {
			adicionarErro("É preciso selecionar um time de coração!");
		}
	}
	
	private void validarNascimento(String nascimento) {
		if(!ValidaUtils.isValid(nascimento)) {
			adicionarErro("A data de nascimento informada não é valida!");
		}
	}


	private void validarNome(String nome) {
		if (StringUtils.isEmpty(nome)) {
			adicionarErro("O nome é obrigatório");
		}
		
		if (nome.length() > 50) {
			adicionarErro("O nome digitado é muito grande");
		}
	}
	
	private void validarEmail(String email) {
		String regex = "[A-Za-z0-9\\._-]+@[A-Za-z0-9]+(\\.[A-Za-z]+)*";

        if(!email.matches(regex)) {
        	adicionarErro("O email digitado não é valido!");
        }
		
	}

	@SuppressWarnings("unchecked")
	private void adicionarErro(String erro) {
		List<String> erros = (List<String>) getRequest().getAttribute("erros");
		if (erros == null) {
			erros = new ArrayList<String>();
			getRequest().setAttribute("erros", erros);
		}

		erros.add(erro);
	}

}
