package campanhas.action;

import campanhas.service.CampanhaService;

/**
 * Exclui uma campanha
 */
public class ExcluirCampanhaAction extends Action {

	@Override
	public void process() throws Exception {

		String campanhaId = getRequest().getParameter("id");
		
		CampanhaService campanhaService = serviceFactory.getService(CampanhaService.class);
		campanhaService.excluir(Integer.parseInt(campanhaId));

		redirect("GerenciarCampanhas.action");
	}
}
