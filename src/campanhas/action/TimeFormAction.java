package campanhas.action;

import campanhas.entity.Time;
import campanhas.service.TimeService;

public class TimeFormAction extends Action{

	@Override
	public void process() throws Exception {
		
		String timeId = getRequest().getParameter("id");
		
		if(timeId != null) {
			TimeService timeService = serviceFactory.getService(TimeService.class);
			Time time = timeService.getTimeById(Integer.parseInt(timeId));
			
			getRequest().setAttribute("id", time.getId());
			getRequest().setAttribute("nome", time.getNome());		
		}
		
		forward("times/time.jsp");
	}

}
