package campanhas.action;

import campanhas.service.TimeService;

/**
 * Exclui um time
 */
public class ExcluirTimeAction extends Action {

	@Override
	public void process() throws Exception {

		String timeId = getRequest().getParameter("id");
		
		TimeService timeService = serviceFactory.getService(TimeService.class);
		timeService.excluir(Integer.parseInt(timeId));

		redirect("ListarTimes.action");
	}
}
