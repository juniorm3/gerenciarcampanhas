package campanhas.action;

import java.util.List;

import campanhas.entity.Time;
import campanhas.entity.Usuario;
import campanhas.service.TimeService;
import campanhas.service.UsuarioService;

public class UsuarioFormAction extends Action{

	@Override
	public void process() throws Exception {
		
		String usuarioId = getRequest().getParameter("id");
		
		TimeService timeService = serviceFactory.getService(TimeService.class);
		List<Time> times = timeService.getTimes();
		
		if(usuarioId != null) {
			
			UsuarioService usuarioService = serviceFactory.getService(UsuarioService.class);
			Usuario usuario = usuarioService.getUsuarioById(Integer.parseInt(usuarioId));
			
			getRequest().setAttribute("id", usuario.getId());
			getRequest().setAttribute("nome", usuario.getNome());
			getRequest().setAttribute("email", usuario.getEmail());
			getRequest().setAttribute("nascimento", usuario.getNascimento());
			getRequest().setAttribute("timeCoracao", usuario.getTimeCoracao().getId());
			getRequest().setAttribute("campanhas", usuario.getCampanhas());
			
		}				
		getRequest().setAttribute("times", times);
		
		forward("usuarios/usuario.jsp");
	}

}
