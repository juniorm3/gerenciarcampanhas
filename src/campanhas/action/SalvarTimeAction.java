package campanhas.action;

import java.util.ArrayList;
import java.util.List;

import campanhas.entity.Time;
import campanhas.service.ServiceException;
import campanhas.service.TimeService;
import campanhas.util.StringUtils;

public class SalvarTimeAction extends Action{

	@Override
	public void process() throws Exception {
		
		/*Recupera dados informados do formulario*/
		String id = getRequest().getParameter("id").trim();
		String nome = getRequest().getParameter("nome").trim();
		
		/*Valida os dados*/
		validarNome(nome);
		
		/*Caso existam erros recoloca os dados na request e retorna para a mesma pagina*/
		if(existemErros()) {
			definirValores(id, nome);
			forward("times/time.jsp");
			return;
		}
		
		Time time = new Time();
		
		if(!StringUtils.isEmpty(id)) {
			time.setId(Integer.parseInt(id));
		}
		time.setNome(nome);
				
		/*Grava dados*/
		TimeService timeService = serviceFactory.getService(TimeService.class);
		timeService.salvar(time);
		
		redirect("GerenciarTimes.action");
		
	}
	
	/**
	 * Recoloca os dados preenchidos na request, para permitir que o formulário 
	 * fique preenchido quando a tela for recarregada
	 * @param id
	 * @param nome
	 * @throws ServiceException 
	 */
	private void definirValores(String id, String nome) throws ServiceException {		
		getRequest().setAttribute("id", id);
		getRequest().setAttribute("nome", nome);
	}

	/**
	 * Verifica se existem erros de validados
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private boolean existemErros() {
		List<String> erros = (List<String>) getRequest().getAttribute("erros");
		if (erros == null || erros.size() == 0) {
			return false;
		}
		return true;
	}


	private void validarNome(String nome) {
		if (StringUtils.isEmpty(nome)) {
			adicionarErro("O nome é obrigatório");
		}
		
		if (nome.length() > 50) {
			adicionarErro("O nome digitado é muito grande");
		}
	}

	@SuppressWarnings("unchecked")
	private void adicionarErro(String erro) {
		List<String> erros = (List<String>) getRequest().getAttribute("erros");
		if (erros == null) {
			erros = new ArrayList<String>();
			getRequest().setAttribute("erros", erros);
		}

		erros.add(erro);

	}

}
