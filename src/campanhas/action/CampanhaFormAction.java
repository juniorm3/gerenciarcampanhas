package campanhas.action;

import java.util.List;

import campanhas.entity.Campanha;
import campanhas.entity.Time;
import campanhas.service.CampanhaService;
import campanhas.service.TimeService;

public class CampanhaFormAction extends Action{

	@Override
	public void process() throws Exception {
		
		String campanhaId = getRequest().getParameter("id");
		
		TimeService timeService = serviceFactory.getService(TimeService.class);
		List<Time> times = timeService.getTimes();
		
		if(campanhaId != null) {
			
			CampanhaService campanhaService = serviceFactory.getService(CampanhaService.class);
			Campanha campanha = campanhaService.getCamapanhaById(Integer.parseInt(campanhaId));			
			
			getRequest().setAttribute("id", campanha.getId());
			getRequest().setAttribute("nome", campanha.getNome());
			getRequest().setAttribute("inicio", campanha.getInicio());
			getRequest().setAttribute("fim", campanha.getFim());
			getRequest().setAttribute("timeCoracao", campanha.getTimeCoracao().getId());			
		}
		
		getRequest().setAttribute("times", times);
		
		forward("campanha/campanha.jsp");
	}

}
