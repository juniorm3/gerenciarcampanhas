package campanhas.action;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import campanhas.entity.Campanha;
import campanhas.entity.Time;
import campanhas.service.CampanhaService;
import campanhas.service.ServiceException;
import campanhas.service.TimeService;
import campanhas.util.StringUtils;
import campanhas.util.ValidaUtils;

public class SalvarCampanhaAction extends Action{

	@Override
	public void process() throws Exception {
		
		/*Recupera dados informados do formulario*/
		String id = getRequest().getParameter("id").trim();
		String nome = getRequest().getParameter("nome").trim();
		String inicio = getRequest().getParameter("dtInicio").trim();
		String fim = getRequest().getParameter("dtFim").trim();
		String timeCoracao = getRequest().getParameter("timeCoracao").trim();
		
		/*Valida os dados*/
		validarNome(nome);
		validarInicio(inicio);
		validarFim(fim);
		validarTime(timeCoracao);
		
		Date dtInicio = null;
		Date dtFim = null;
		if(!existemErros()) {
			dtInicio = StringUtils.stringToDate(inicio, "dd/MM/yyyy");
			dtFim = StringUtils.stringToDate(fim, "dd/MM/yyyy");
			if(dtInicio.after(dtFim) || dtFim.before(dtInicio)) {
				adicionarErro("O período infomado para inicio e fim não é valida!");
			}	
		}
		
		/*Caso existam erros recoloca os dados na request e retorna para a mesma pagina*/
		if(existemErros()) {
			definirValores(id, nome, dtInicio, inicio, dtFim, fim, timeCoracao);
			forward("campanha/campanha.jsp");
			return;
		}
		
		Campanha campanha = new Campanha();
		
		if(!StringUtils.isEmpty(id)) {
			campanha.setId(Integer.parseInt(id));
		}
		campanha.setNome(nome);
		
		
		campanha.setInicio(dtInicio);
		campanha.setFim(dtFim);
		
		
		TimeService timeService = serviceFactory.getService(TimeService.class);
		Time time = timeService.getTimeById(Integer.parseInt(timeCoracao));
		campanha.setTimeCoracao(time);
				
		/*Grava dados*/
		CampanhaService campanhaService = serviceFactory.getService(CampanhaService.class);
		campanhaService.salvar(campanha);
		
		redirect("GerenciarCampanhas.action");
		
	}
	
	/**
	 * Recoloca os dados preenchidos na request, para permitir que o formulário 
	 * fique preenchido quando a tela for recarregada
	 * @param id
	 * @param nome
	 * @param dtInicio
	 * @param inicio 
	 * @param dtFim
	 * @param timeCoracao
	 * @param timeCoracao2 
	 * @throws ServiceException 
	 * @throws ParseException 
	 */
	private void definirValores(String id, String nome, Date dtInicio, String inicio, Date dtFim, 
			String fim, String timeCoracao) throws ServiceException, ParseException {	
		
		getRequest().setAttribute("id", id);
		getRequest().setAttribute("nome", nome);
		if(dtInicio == null) {
			getRequest().setAttribute("inicio", inicio.equals("") ? null : StringUtils.stringToDate(inicio, "dd/MM/yyyy"));
		} else {
			getRequest().setAttribute("inicio", dtInicio);	
		}
		
		if(dtFim == null) {
			getRequest().setAttribute("fim", fim.equals("") ? null : StringUtils.stringToDate(fim, "dd/MM/yyyy"));
		} else {
			getRequest().setAttribute("fim", dtFim);
		}
		
		getRequest().setAttribute("timeCoracao", Integer.parseInt(timeCoracao));

		TimeService timeService = serviceFactory.getService(TimeService.class);
		List<Time> times = timeService.getTimes();
		getRequest().setAttribute("times", times);
	}

	/**
	 * Verifica se existem erros de validados
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private boolean existemErros() {
		List<String> erros = (List<String>) getRequest().getAttribute("erros");
		if (erros == null || erros.size() == 0) {
			return false;
		}
		return true;
	}


	private void validarTime(String timeCoracao) {
		Integer id = Integer.parseInt(timeCoracao);
		if(id == -1) {
			adicionarErro("É preciso selecionar um time de coração!");
		}
	}

	private void validarInicio(String inicio) {
		if(!ValidaUtils.isValid(inicio)) {
			adicionarErro("A data de inicio informada não é valida!");
		} else if(inicio == null || inicio.equals("")){
			adicionarErro("A data de inicio não foi preenchida!");
		} else {
			Date dtInicio = null;
			Date dtHoje = null;
			Date hoje = new Date();
			try {
				dtInicio = StringUtils.stringToDate(inicio, "dd/MM/yyyy");
				dtHoje = StringUtils.stringToDate(StringUtils.convertDate(hoje, "dd/MM/yyyy"), "dd/MM/yyyy");
			} catch (ParseException e) {
				e.printStackTrace();
			}

			if(dtInicio.before(dtHoje)) {
				adicionarErro("A data de inicio não pode ser menor do que hoje!");
			}
		}		
				
	}
	
	private void validarFim(String fim) {
		if(!ValidaUtils.isValid(fim)) {
			adicionarErro("A data de fim informada não é valida!");
		} else if(fim == null || fim.equals("")){
			adicionarErro("A data de fim não foi preenchida!");
		} else {
			Date hoje = new Date();
			try {
				Date dtFim = StringUtils.stringToDate(fim, "dd/MM/yyyy");
				if(dtFim.before(hoje)) {
					adicionarErro("A data de fim não pode ser menor do que hoje!");
				}
			} catch (ParseException e) {			
				e.printStackTrace();
			}
		}
	}


	private void validarNome(String nome) {
		if (StringUtils.isEmpty(nome)) {
			adicionarErro("O nome é obrigatório");
		}
		
		if (nome.length() > 50) {
			adicionarErro("O nome digitado é muito grande");
		}
	}

	@SuppressWarnings("unchecked")
	private void adicionarErro(String erro) {
		List<String> erros = (List<String>) getRequest().getAttribute("erros");
		if (erros == null) {
			erros = new ArrayList<String>();
			getRequest().setAttribute("erros", erros);
		}

		erros.add(erro);

	}

}
