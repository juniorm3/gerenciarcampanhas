package campanhas.action;

import java.util.List;

import campanhas.entity.Time;
import campanhas.service.TimeService;

/**
 * Lista os times cadastrados
 */
public class ListarTimesAction extends Action {

	@Override
	public void process() throws Exception {
		
		getSession().setAttribute("menuAtivo", "times");
		
		TimeService tomeService = serviceFactory.getService(TimeService.class);
		List<Time> times = tomeService.getTimes();
		
		getRequest().setAttribute("times", times);
		
		forward("times/lista_times.jsp");
	}
}
