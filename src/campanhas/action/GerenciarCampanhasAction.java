package campanhas.action;

import java.util.List;

import campanhas.entity.Campanha;
import campanhas.service.CampanhaService;

public class GerenciarCampanhasAction extends Action {

	@Override
	public void process() throws Exception {
		
		getSession().setAttribute("menuAtivo", "gerenciar");
		
		CampanhaService campanhaService = serviceFactory.getService(CampanhaService.class);
		List<Campanha> campanhas = campanhaService.getCampanhas();
		
		getRequest().setAttribute("campanhas", campanhas);
		
		forward("campanha/lista_campanhas.jsp");
		
	}

}
