package campanhas.action;

import campanhas.service.UsuarioService;

/**
 * Exclui um usuário
 */
public class ExcluirUsuarioAction extends Action {

	@Override
	public void process() throws Exception {

		String usuarioId = getRequest().getParameter("id");
		
		UsuarioService usuarioService = serviceFactory.getService(UsuarioService.class);
		usuarioService.excluir(Integer.parseInt(usuarioId));

		redirect("GerenciarUsuario.action");
	}
}
