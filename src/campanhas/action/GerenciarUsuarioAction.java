package campanhas.action;

import java.util.List;

import campanhas.entity.Usuario;
import campanhas.service.UsuarioService;

public class GerenciarUsuarioAction extends Action {

	@Override
	public void process() throws Exception {
		
		getSession().setAttribute("menuAtivo", "usuarios");
		
		UsuarioService usuarioService = serviceFactory.getService(UsuarioService.class);
		List<Usuario> usuarios = usuarioService.getUsuarios();
		
		getRequest().setAttribute("usuarios", usuarios);
		
		forward("usuarios/lista_usuarios.jsp");
		
	}

}
