package campanhas.entity;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

public class Usuario {

	private Integer id;
	private String nome;
	private String email;
	private Date nascimento;
	private Time timeCoracao;
	private Set<Campanha> campanhas = new LinkedHashSet<Campanha>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getNascimento() {
		return nascimento;
	}

	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}

	public Time getTimeCoracao() {
		return timeCoracao;
	}

	public void setTimeCoracao(Time timeCoracao) {
		this.timeCoracao = timeCoracao;
	}

	public Set<Campanha> getCampanhas() {
		return campanhas;
	}

	public void setCampanhas(Set<Campanha> campanhas) {
		this.campanhas = campanhas;
	}

}
