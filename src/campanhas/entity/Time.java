package campanhas.entity;

import java.util.LinkedHashSet;
import java.util.Set;

public class Time {

	private Integer id;
	private String nome;
	private Set<Campanha> campanhas = new LinkedHashSet<Campanha>();
	private Set<Usuario> usuarios = new LinkedHashSet<Usuario>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Set<Campanha> getCampanhas() {
		return campanhas;
	}

	public void setCampanhas(Set<Campanha> campanhas) {
		this.campanhas = campanhas;
	}

	public Set<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

}
