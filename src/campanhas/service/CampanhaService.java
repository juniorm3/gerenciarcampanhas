package campanhas.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import campanhas.dao.CampanhaDAO;
import campanhas.dao.DAOException;
import campanhas.entity.Campanha;
import webf.util.StringUtils;

public class CampanhaService extends Service {

	/**
	 * Serviço responsavel por chamar a execuçao de salvar ou atualizar
	 * 
	 * @param campanha
	 * @throws ServiceException
	 */
	public void salvar(Campanha campanha) throws ServiceException {
		try {
			CampanhaDAO campanhaDAO = daoFactory.getDAO(CampanhaDAO.class);
			// Verificar e adicionar um dia as campanhas no mesmo periodo
			/*List<Campanha> campanhasAtivas = campanhaDAO.getCampanhaByNomeAtivas(campanha.getInicio());
			if(campanhasAtivas.size() > 0) {
				atualizarDatas(campanhasAtivas);
			}*/
						
			if (campanha.getId() == null) {
				campanhaDAO.save(campanha);
			} else {
				campanhaDAO.update(campanha);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * Atualizar data de encerramento das campanhas
	 * @param campanhasAtivas
	 * @throws DAOException
	 */
	private void atualizarDatas(List<Campanha> campanhasAtivas) throws DAOException {		
		for(Campanha campanha : campanhasAtivas) {
			Calendar c = Calendar.getInstance();
			c.setTime(campanha.getFim());
			c.add(Calendar.DATE, 1);			
			campanha.setFim(c.getTime());
			CampanhaDAO campanhaDAO = daoFactory.getDAO(CampanhaDAO.class);
			campanhaDAO.update(campanha);
		}
		
	}

	/**
	 * Serviço responsável por deletar o registro
	 * 
	 * @param campanhaId
	 * @throws ServiceException
	 */
	public void excluir(Integer campanhaId) throws ServiceException {
		try {
			CampanhaDAO campanhaDAO = daoFactory.getDAO(CampanhaDAO.class);

			Campanha usuario = campanhaDAO.load(campanhaId);
			campanhaDAO.delete(usuario);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * Serviço responsavél por pesquisar campanhas
	 * 
	 * @param nome
	 * @return
	 * @throws ServiceException
	 */
	public List<Campanha> pesquisarCampanhas(String nome) throws ServiceException {
		try {
			List<Campanha> campanhas = new ArrayList<Campanha>();

			CampanhaDAO campanhaDAO = daoFactory.getDAO(CampanhaDAO.class);

			if (!StringUtils.isEmpty(nome)) {
				campanhas.addAll(campanhaDAO.getCampanhaByNome(nome));
			}

			return new ArrayList<Campanha>(campanhas);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
		

	/**
	 * SErviço responsável por resgatar uma campanha pelo id informadao
	 * 
	 * @param id
	 * @return
	 * @throws ServiceException
	 */
	public Campanha getCamapanhaById(Integer id) throws ServiceException {
		try {
			CampanhaDAO campanhaDAO = daoFactory.getDAO(CampanhaDAO.class);
			return campanhaDAO.getCampanhaById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * Serviço reposável por recuperar as campanhas cadastradas
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public List<Campanha> getCampanhas() throws ServiceException {
		try {
			CampanhaDAO campanhaDAO = daoFactory.getDAO(CampanhaDAO.class);
			return campanhaDAO.getCampanhas();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Serviço responsável por trazer apenas as campanhas não vencidas
	 * @return
	 * @throws ServiceException
	 */
	public List<Campanha> getCampanhasValidas() throws ServiceException {
		try {
			CampanhaDAO campanhaDAO = daoFactory.getDAO(CampanhaDAO.class);
			return campanhaDAO.getCampanhasAtivas();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Serviço responsavél por pesquisar campanhas
	 * 
	 * @param nome
	 * @return
	 * @throws ServiceException
	 */
	public List<Campanha> pesquisarCampanhasValidas(String nome) throws ServiceException {
		try {
			List<Campanha> campanhas = new ArrayList<Campanha>();

			CampanhaDAO campanhaDAO = daoFactory.getDAO(CampanhaDAO.class);

			if (!StringUtils.isEmpty(nome)) {
				campanhas.addAll(campanhaDAO.getCampanhaByNomeAtivas(nome));
			}

			return new ArrayList<Campanha>(campanhas);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
