package campanhas.service;

import java.util.ArrayList;
import java.util.List;

import campanhas.dao.DAOException;
import campanhas.dao.TimeDAO;
import campanhas.entity.Time;
import webf.util.StringUtils;

public class TimeService extends Service{
	
	public void salvar(Time time) throws ServiceException {
		try {
			TimeDAO timeDAO = daoFactory.getDAO(TimeDAO.class);

			if (time.getId() == null) {
				timeDAO.save(time);
			} else {
				timeDAO.update(time);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void excluir(Integer timeId) throws ServiceException {
		try {
			TimeDAO timeDAO = daoFactory.getDAO(TimeDAO.class);

			Time time = timeDAO.load(timeId);
			timeDAO.delete(time);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<Time> pesquisarTimes(String nome) throws ServiceException {
		try {
			List<Time> times = new ArrayList<Time>();

			TimeDAO timeDAO = daoFactory.getDAO(TimeDAO.class);

			if (!StringUtils.isEmpty(nome)) {
				times.addAll(timeDAO.getTimesByNome(nome));
			}

			return times;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public Time getTimeById(Integer id) throws ServiceException {
		try {
			TimeDAO timeDAO = daoFactory.getDAO(TimeDAO.class);
			return timeDAO.getTimeById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<Time> getTimes() throws ServiceException {
		try {
			TimeDAO timeDAO = daoFactory.getDAO(TimeDAO.class);
			return timeDAO.getTimes();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
