package campanhas.service;

import java.util.ArrayList;
import java.util.List;

import campanhas.dao.DAOException;
import campanhas.dao.UsuarioDAO;
import campanhas.entity.Usuario;
import webf.service.ServiceException;
import webf.util.StringUtils;

public class UsuarioService extends Service {

	public void salvar(Usuario usuario) throws ServiceException {
		try {
			UsuarioDAO usuarioDAO = daoFactory.getDAO(UsuarioDAO.class);

			if (usuario.getId() == null) {
				usuarioDAO.save(usuario);
			} else {
				usuarioDAO.update(usuario);
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void excluir(Integer usuarioId) throws ServiceException {
		try {
			UsuarioDAO usuarioDAO = daoFactory.getDAO(UsuarioDAO.class);

			Usuario usuario = usuarioDAO.load(usuarioId);
			usuarioDAO.delete(usuario);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<Usuario> pesquisarUsuarios(String nome) throws ServiceException {
		try {
			List<Usuario> usuarios = new ArrayList<Usuario>();

			UsuarioDAO usuarioDAO = daoFactory.getDAO(UsuarioDAO.class);

			if (!StringUtils.isEmpty(nome)) {
				usuarios.addAll(usuarioDAO.getUsuariosByNome(nome));
			}

			return usuarios;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public Usuario getUsuarioById(Integer id) throws ServiceException {
		try {
			UsuarioDAO usuarioDAO = daoFactory.getDAO(UsuarioDAO.class);
			return usuarioDAO.getUsuarioById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<Usuario> getUsuarios() throws ServiceException {
		try {
			UsuarioDAO usuarioDAO = daoFactory.getDAO(UsuarioDAO.class);
			return usuarioDAO.getUsuarios();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * Efetua o login de um usuário
	 * @param email
	 * @return
	 * @throws ServiceException 
	 */
	public Usuario login(String email) throws ServiceException {
		try {
			UsuarioDAO usuarioDAO = daoFactory.getDAO(UsuarioDAO.class);
			
			Usuario usuario = usuarioDAO.getUsuariosByEmail(email);
			
			if (usuario == null) {
				return null;
			}
			
			if(!usuario.getEmail().equals(email)) {
				return null;
			}
			
			return usuario;
			
		}catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
