package campanhas.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import campanhas.util.HibernateUtil;

public abstract class DAO<T> {

	private Session session;
	private Class<T> clazz;

	protected DAO(Class<T> clazz) {
		session = HibernateUtil.getSession();
		this.clazz = clazz;
	}

	@SuppressWarnings("unchecked")
	public T load(Serializable id) throws DAOException {
		try {
			session.beginTransaction();
			return (T) session.load(clazz, id);
		} catch (HibernateException e) {
			throw new DAOException(e);
		} 
	}

	public void save(T obj) throws DAOException {
		try {
			session.beginTransaction();
			session.save(obj);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			throw new DAOException(e);
		} 
	}

	public void delete(T obj) throws DAOException {
		try {
			session.beginTransaction();
			session.delete(obj);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
	}

	public void update(T obj) throws DAOException {
		try {
			session.beginTransaction();
			/*session.update(obj);*/ 
			session.merge(obj);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			throw new DAOException(e);
		} 
	}

	protected List<?> list(String hql) throws DAOException {
		try {
			Query q = query(hql);
			return q.list();
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
	}

	@SuppressWarnings("unchecked")
	protected T result(String hql, Class<T> clazz) throws DAOException {
		List<?> results = list(hql);
		if (results.size() == 0) {
			return null;
		}
		return (T) results.get(0);
	}

	protected Query query(String hql) throws DAOException {
		try {
			session.beginTransaction();
			return session.createQuery(hql);
		} catch (HibernateException e) {
			throw new DAOException(e);
		}
	}
}
