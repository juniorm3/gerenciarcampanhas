package campanhas.dao;

import java.util.List;

import org.hibernate.Query;

import campanhas.entity.Time;

@SuppressWarnings("unchecked")
public class TimeDAO extends DAO<Time> {

	protected TimeDAO() {
		super(Time.class);		
	}
	
	public List<Time> getTimesByNome(String nome) throws DAOException {
		Query q = query("FROM Time t WHERE upper(t.nome) like :nome order by t.nome");
		q.setParameter("nome", "%" + nome.toUpperCase() + "%");
		return q.list();
	}
	
	public List<Time> getTimes() throws DAOException {
		return (List<Time>) list("FROM Time t ORDER BY t.nome");
	}
	
	public Time getTimeById(Integer id) throws DAOException {
		Query q = query("FROM Time t WHERE t.id = :id");
		q.setParameter("id", id);
		return (Time) q.uniqueResult();
	}

}
