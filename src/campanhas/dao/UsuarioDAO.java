package campanhas.dao;

import java.util.List;

import org.hibernate.Query;

import campanhas.entity.Usuario;

@SuppressWarnings("unchecked")
public class UsuarioDAO extends DAO<Usuario> {

	protected UsuarioDAO() {
		super(Usuario.class);
	}

	public List<Usuario> getUsuariosByNome(String nome) throws DAOException {
		Query q = query("FROM Usuario u WHERE upper(u.nome) like :nome order by u.nome");
		q.setParameter("nome", "%" + nome.toUpperCase() + "%");
		return q.list();
	}

	public List<Usuario> getUsuarios() throws DAOException {
		return (List<Usuario>) list("FROM Usuario u ORDER BY u.nome");
	}
	
	public Usuario getUsuarioById(Integer id) throws DAOException {
		Query q = query("FROM Usuario u WHERE u.id = :id");
		q.setParameter("id", id);
		return (Usuario) q.uniqueResult();
	}

	/**
	 * Otem usuario com base no e-mail
	 * @param email
	 * @return
	 * @throws DAOException 
	 */
	public Usuario getUsuariosByEmail(String email) throws DAOException {
		Query q = query("FROM Usuario u WHERE upper(u.email) = :email");
		q.setParameter("email", email.toUpperCase());
		return (Usuario) q.uniqueResult();
	}

}
