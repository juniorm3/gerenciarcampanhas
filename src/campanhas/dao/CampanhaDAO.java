package campanhas.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import campanhas.entity.Campanha;
import campanhas.util.HibernateUtil;

@SuppressWarnings("unchecked")
public class CampanhaDAO extends DAO<Campanha> {

	protected CampanhaDAO() {
		super(Campanha.class);
	}

	public List<Campanha> getCampanhaByNome(String nome) throws DAOException {
		Query q = query("FROM Campanha c WHERE upper(c.nome) like :nome order by c.nome");
		q.setParameter("nome", "%" + nome.toUpperCase() + "%");
		return q.list();
	}
	
	public List<Campanha> getCampanhas() throws DAOException {
		return (List<Campanha>) list("FROM Campanha c ORDER BY c.inicio, c.fim");
	}
	
	public List<Campanha> getCampanhasAtivas() throws DAOException {
		Date now = new Date();
		Query q = query("FROM Campanha c WHERE c.fim > :now");
		q.setTimestamp("now", now);
		return q.list();
	}
	
	public Campanha getCampanhaById(Integer id) throws DAOException {
		Query q = query("FROM Campanha c WHERE c.id = :id");
		q.setParameter("id", id);
		return (Campanha) q.uniqueResult();
	}
	
	public List<Campanha> getCampanhaByNomeAtivas(String nome) throws DAOException {
		Date now = new Date();
		Query q = query("FROM Campanha c WHERE upper(c.nome) like :nome AND c.fim > :now order by c.inicio");
		q.setParameter("nome", "%" + nome.toUpperCase() + "%");
		q.setTimestamp("now", now);
		return q.list();
	}
	
	/**
	 * Verifica se uma data esta dentro da faixa das dastas de inicio e fim das campanhas
	 * @param data
	 * @return
	 * @throws DAOException
	 */
	public List<Campanha> getCampanhaByNomeAtivas(Date data) throws DAOException {
		Date now = new Date();
		Query q = query("FROM Campanha c WHERE c.fim > :now AND :data BETWEEN c.inicio AND c.fim");		
		q.setTimestamp("now", now);
		q.setDate("data", data);
		return q.list();
	}

}
